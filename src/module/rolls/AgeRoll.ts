import { PlayerCharacterActor } from "../actors/PlayerCharacterActor";
import { AbilityScore } from "../data/Actor/shared/Abilities";
import { FocusData } from "../data/Item/FocusData";
import { focusType, weaponType, arcanaType } from "../data/Item/ItemTypes";
import { BaseItemData } from "../data/Item/BaseItemData";
import { WeaponData } from "../data/Item/WeaponData";
import { ArcanaData } from "../data/Item/ArcanaData";

export class AgeRoll extends Roll {
    name: string;
    focusBonus: number;
    abilityBonus: number;
    damage?: string;

    static getFocusBonus(items: Collection<Item>, focus: string) {
        var focusItem = items.find((i: Item<FocusData>) => i.type == focusType && i.name == focus) as Item<FocusData>;
        if (focusItem != undefined) {
            return focusItem.data.data.improved ? 3 : 2;
        } else {
            return 0;
        }
    }

    static readonly toModString = (value: number) =>
        value == 0 ? "" : value > 0 ? `+ ${value}` : `- ${Math.abs(value)}`;

    constructor(roller: PlayerCharacterActor, data: Item<BaseItemData> | string, damage?: string) {
        let ability: AbilityScore;
        let focus: string;
        if (typeof data == "string") {
            ability = data;
            focus = null;
        } else {
            switch (data.data.type) {
                case weaponType:
                    var wd = data.data.data as WeaponData;
                    ability = wd.defaultAbility;
                    focus = wd.focus;
                    break;
                case focusType:
                    var fd = data.data.data as FocusData;
                    ability = fd.defaultAbility;
                    focus = data.name;
                    break;
                case arcanaType:
                    var ad = data.data.data as ArcanaData;
                    ability = ad.ability;
                    focus = ad.origin;
                    break;
            }
        }

        const focusBonus = AgeRoll.getFocusBonus(roller.items, focus);
        const abilityBonus = roller.data.data.abilities[ability];
        super(`3d6 + ${focusBonus} + ${abilityBonus}`);
        this.focusBonus = focusBonus;
        this.abilityBonus = abilityBonus;
        this.name = `${ability}${focus != undefined ? ` (${focus})` : ""}`;
        this.damage = damage;
    }

    async render(chatOptions: any = {}) {
        chatOptions = mergeObject(
            {
                user: game.user._id,
                flavor: null,
                template: "systems/blue-rose/templates/chat/ageRoll.hbs",
            },
            chatOptions || {}
        );

        if (!this._rolled) {
            this.roll();
        }
        var rolls = this.dice[0].rolls.map((r) => r.roll);
        var match = new Set(rolls).size !== 3;
        var damageResult = this.damage
            ? await new Roll(this.damage, { flavor: "Damage:" }).render()
            : undefined

        const chatData = {
            user: chatOptions.user,
            rolls: rolls.reduce((p, c, i) => ({ ...p, [`d${i}`]: c }), new Map<string, number>()),
            name: this.name,
            focusBonus: AgeRoll.toModString(this.focusBonus),
            abilityBonus: AgeRoll.toModString(this.abilityBonus),
            total: this.total,
            stuntPoints: match ? rolls[2] : 0,
            damageResult
        };

        return renderTemplate(chatOptions.template, chatData);
    }
    async toMessage(chatData) {
        chatData.content = await this.render({ user: chatData.user });
        return ChatMessage.create(chatData);
    }
}
