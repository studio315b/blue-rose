import { AbilityScore } from "../Actor/shared/Abilities";

export type RollableItemData = {
    defaultAbility: AbilityScore;
};
