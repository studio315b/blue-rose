import { PlayerCharacterActor } from "../actors/PlayerCharacterActor";
import { PlayerCharacterData } from "../data/Actor/PlayerCharacterData";
import { BaseItemData } from "../data/Item/BaseItemData";
import {
    weaponType,
    arcanaType,
    talentType,
    shieldType,
    armorType,
    physicalItemType,
    focusType,
} from "../data/Item/ItemTypes";
import { ArmorData, emptyArmor } from "../data/Item/ArmorData";
import { AgeRoll } from "../rolls/AgeRoll";
import { WeaponData } from "../data/Item/WeaponData";

const actionTypes = [weaponType, arcanaType];
const talentTypes = [talentType];
const gearTypes = [shieldType, armorType, physicalItemType];
const focusTypes = [focusType];

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class PlayerCharacterSheet extends ActorSheet<
    PlayerCharacterActor,
    PlayerCharacterData
    > {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["blue-rose", "sheet", "player-character"],
            template: "systems/blue-rose/templates/sheets/playerCharacterSheet.hbs",
            width: 600,
            height: 600,
            tabs: [
                {
                    navSelector: ".sheet-tabs",
                    contentSelector: ".sheet-body",
                    initial: "main",
                },
            ],
            dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
        });
    }

    /** @override */
    activateListeners(html: JQuery) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Update Inventory Item
        html.find(".item-edit").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        // Delete Inventory Item
        html.find(".item-delete").click((ev) => {
            const li = $(ev.currentTarget).parents(".item");
            this.actor.deleteOwnedItem(li.data("itemId"));
            li.slideUp(200, () => this.render(false));
        });

        html
            .find(".relationship-add")
            .click(this._onClickRelationshipControl.bind(this));
        html
            .find(".relationship-delete")
            .click(this._onClickRelationshipControl.bind(this));

        html.find(".rollable").click(async (ev) => {
            let target = $(ev.currentTarget).data();
            let item = target.itemId ? this.actor.items.get(target.itemId) : null;
            let damage = item
                ? (this.actor.items.get(target.itemId).data.data as WeaponData).damage
                : undefined;
            let roll = new AgeRoll(this.actor, item ? item : target.ability, damage);
            await roll.render().then((content) => {
                ChatMessage.create({
                    user: game.user._id,
                    speaker: ChatMessage.getSpeaker({ actor: this.actor }),
                    content,
                });
            });
        });
    }

    getData(): ActorSheetData {
        const sheetData = super.getData();

        const armor = (sheetData.items || [])
            .filter((i: ItemSheetData<BaseItemData>) => i.data.type == armorType)
            .filter((i: ItemSheetData<ArmorData>) => i.data.equipped)
            .map((i: ItemSheetData<ArmorData>) => i.data)
            .reduce((c, v) => (c.armorRating > v.armorRating ? c : v), emptyArmor);

        sheetData.data.armorRating = armor.armorRating;
        sheetData.data.armorPenalty = armor.armorPenalty;
        this._sortItems(sheetData);
        while (sheetData.data.persona.goals.length < 3) {
            sheetData.data.persona.goals.push("");
        }

        return sheetData;
    }

    /** @override */
    setPosition(options = {}) {
        const position = super.setPosition(options);
        const sheetBody = this.element.find(".sheet-body");
        const bodyHeight = position.height - 150;
        sheetBody.css("height", bodyHeight);
        return position;
    }

    async _onClickRelationshipControl(event: JQuery.ClickEvent) {
        event.preventDefault();
        const a = event.currentTarget;
        const action = a.dataset.action;
        const relationships = this.object.data.data.relationships;

        if (action == "add") {
            const nk =
                Object.keys(relationships).reduce(
                    (p, c) => Math.max(p, parseInt(c)),
                    0
                ) + 1;
            let newKey = document.createElement("input");
            newKey.type = "text";
            newKey.name = `data.relationships.${nk}.name`;
            newKey.value = "New Relationship";
            this.form.appendChild(newKey);
            await this._onSubmit(event);
        }

        if (action == "remove") {
            $(`input[name^="data.relationships.${a.dataset.index}"]`).remove();
            await this._onSubmit(event);
        }
    }

    _sortItems(sheetData: ActorSheetData) {
        const actions = sheetData.items.filter((i) =>
            actionTypes.includes(i.data.type as any)
        );
        const talents = sheetData.items.filter((i) =>
            talentTypes.includes(i.data.type as any)
        );
        const gear = sheetData.items.filter((i) =>
            gearTypes.includes(i.data.type as any)
        );
        const focuses = sheetData.items.filter((i) =>
            focusTypes.includes(i.data.type as any)
        );

        (sheetData as any).itemGroups = {
            actions,
            talents,
            gear,
            focuses,
        };
    }

    /** @override */
    _updateObject(
        event: Event | null,
        formData: any
    ): Promise<PlayerCharacterActor> {
        const relationships = expandObject(formData).data.relationships || [];
        for (let k of Object.keys(this.object.data.data.relationships)) {
            if (!relationships.hasOwnProperty(k)) relationships[`-=${k}`] = null;
        }

        // Re-combine formData
        formData = Object.entries(formData).reduce<any>(
            (obj, e) => {
                obj[e[0]] = e[1];
                return obj;
            },
            {
                _id: this.object._id,
                data: { relationships },
            }
        );

        // Update the Actor
        return this.object.update(formData) as any;
    }
}
